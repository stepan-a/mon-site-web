(add-to-list 'load-path "/usr/local/share/emacs/site-lisp/org-mode")
(add-to-list 'load-path "/home/stepan/Sources/org-mode/contrib/lisp")

(setq org-export-htmlize-output-type 'css)

(require 'org)
(require 'org-publish)
(require 'org-special-blocks)

(setq org-publish-project-alist
      '(
        ("homepage-notes"
         :base-directory "~/Web/org/"
         :base-extension "org"
         :publishing-directory "~/Web/www/"
         :recursive t
         :publishing-function org-publish-org-to-html
         :headline-levels 4             ; Just the default for this project.
         :auto-preamble t
         )
        ("homepage-static"
         :base-directory "~/Web/org/"
         :base-extension "tar.bz2\\|dat\\|mat\\b|csv\\|mod\\|asc\\|css\\|js\\|png\\|bz2\\|eps\\|jpg\\|gif\\|pdf\\|ps\\|tex\\|cc\\|R\\|m"
         :publishing-directory "~/Web/www/"
         :recursive t
         :publishing-function org-publish-attachment
         )
        ("homepage" :components ("homepage-notes" ));"homepage-static"
      )
)
